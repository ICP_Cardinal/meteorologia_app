package com.example.miaplicacion2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView clChillan;
    private TextView clChillantemp;
    private TextView clChillanhum;
    private TextView clChillanpre;
    private TextView clMoscow;
    private TextView clMoscowtemp;
    private TextView clMoscowhum;
    private TextView clMoscowpre;
    private TextView clMadrid;
    private TextView clMadridtemp;
    private TextView clMadridhum;
    private TextView clMadridpre;
    private TextView clParis;
    private TextView clParistemp;
    private TextView clParishum;
    private TextView clParispre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.clChillan = (TextView) findViewById(R.id.clChillan);
        this.clChillantemp = (TextView) findViewById(R.id.clChillantemp);
        this.clChillanhum = (TextView) findViewById(R.id.clChillanhum);
        this.clChillanpre = (TextView) findViewById(R.id.clChillanpre);
        this.clMoscow = (TextView) findViewById(R.id.clMoscow);
        this.clMoscowtemp = (TextView) findViewById(R.id.clMoscowtemp);
        this.clMoscowhum = (TextView) findViewById(R.id.clMoscowhum);
        this.clMoscowpre = (TextView) findViewById(R.id.clMoscowpre);
        this.clMadrid = (TextView) findViewById(R.id.clMadrid);
        this.clMadridtemp = (TextView) findViewById(R.id.clMadridtemp);
        this.clMadridhum = (TextView) findViewById(R.id.clMadridhum);
        this.clMadridpre = (TextView) findViewById(R.id.clMadridpre);
        this.clParis = (TextView) findViewById(R.id.clParis);
        this.clParistemp = (TextView) findViewById(R.id.clParistemp);
        this.clParishum = (TextView) findViewById(R.id.clParishum);
        this.clParispre = (TextView) findViewById(R.id.clParispre);

        String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6067&lon=-72.1034&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humidity = mainJSON.getDouble("humidity");
                            double pressure = mainJSON.getDouble("pressure");
                            clChillan.setText("Chillán");
                            clChillantemp.setText("Temperatura " + temp + "° C");
                            clChillanhum.setText("Humedad " + humidity + "%");
                            clChillanpre.setText("Presión " + pressure + " hPa");



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);

        String url2 = "http://api.openweathermap.org/data/2.5/weather?lat=55.751244&lon=37.618423&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        StringRequest solicitud2 = new StringRequest(
                Request.Method.GET,
                url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humidity = mainJSON.getDouble("humidity");
                            double pressure = mainJSON.getDouble("pressure");
                            clMoscow.setText("Moscú");
                            clMoscowtemp.setText("Temperatura " + temp + "° C");
                            clMoscowhum.setText("Humedad " + humidity + "%");
                            clMoscowpre.setText("Presión " + pressure + " hPa");



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEspera2 = Volley.newRequestQueue(getApplicationContext());
        listaEspera2.add(solicitud2);

        String url3 = "http://api.openweathermap.org/data/2.5/weather?lat=40.4167&lon=-3.70325&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        StringRequest solicitud3 = new StringRequest(
                Request.Method.GET,
                url3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humidity = mainJSON.getDouble("humidity");
                            double pressure = mainJSON.getDouble("pressure");
                            clMadrid.setText("Madrid");
                            clMadridtemp.setText("Temperatura " + temp + "° C");
                            clMadridhum.setText("Humedad " + humidity + "%");
                            clMadridpre.setText("Presión " + pressure + " hPa");



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEspera3 = Volley.newRequestQueue(getApplicationContext());
        listaEspera3.add(solicitud3);

        String url4 = "http://api.openweathermap.org/data/2.5/weather?lat=48.8667&lon=2.33333&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        StringRequest solicitud4 = new StringRequest(
                Request.Method.GET,
                url4,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humidity = mainJSON.getDouble("humidity");
                            double pressure = mainJSON.getDouble("pressure");
                            clParis.setText("París");
                            clParistemp.setText("Temperatura " + temp + "° C");
                            clParishum.setText("Humedad " + humidity + "%");
                            clParispre.setText("Presión " + pressure + " hPa");



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEspera4 = Volley.newRequestQueue(getApplicationContext());
        listaEspera4.add(solicitud4);
    }
}
